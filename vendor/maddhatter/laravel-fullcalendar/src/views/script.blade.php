<script>
    $(document).ready(function() {


        $('#calendar-{{ $id }}').fullCalendar( {
            timeFormat: 'HH(:mm)',
            weekNumbers: true,
            selectable: true,
            nowIndicator: true,


            eventClick: function (event) {
                var deleteMsg = confirm("Confirma Apagar:  " + event.title + "?");
                if (deleteMsg) {
                    $.ajax({
                        type: "POST",
     // Nao esta redirecionando para web.php para pegar a rota e levar para o controller
                        url:  "{{ url('/eventos/apagaevento') }}",
                        data: "id=" + event._id,
                        success: function (response) {
                            if(parseInt(response) > 0) {
                             //   $('#calendar').fullCalendar('removeEvents', event.id);
                                displayMessage("Deleted Successfully");
                            }
                        }
                    });
                }
            },

            dayClick: function(date) {
                alert('clicked ' + date.format('DD.MM.YYYY HH:mm'));
            },
            header: {
                left: 'month, today, agendaWeek,agendaDay custom1',
                center: 'title',
                right: 'prevYear,prev,next,nextYear'
            },
            footer: {
                left: 'custom1',
                center: '',
                right: 'prev,next'
            },
            customButtons: {
                custom1: {
                    text: 'custom 1',
                    click: function() {
                        alert('clicked custom button 1!');
                    }
                },

            },

            events:  {!! $options !!}

        } );

   });
</script>
