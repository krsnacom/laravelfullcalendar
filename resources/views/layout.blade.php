
<!doctype html>
  <html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1">
    
        @yield('style')
      
        <title>Calendario</title>
      

    </head>
    
  <body>
  
    @yield('content')
  
  </body>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="fullcalendar.js"></script>
    <script src='locale/pt-br.js'></script>

    {!! $calendar->script() !!}

</html>
