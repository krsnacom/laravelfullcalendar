@extends('layout')

@section('style')
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="fullcalendar.css"/>


@endsection


@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <div class="panel-heading">Calendar Example</div>
          
          <div class="panel-body">
            {!! $calendar->calendar() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https:jquery-ui-1.12.1/jquery-ui.min.js"> </script>

  {!! $calendar->script() !!}

@endsection