<!doctype html>
  <html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1">
    
       
@section('style')
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="fullcalendar.css"/>
@endsection
      
        <title>Calendario</title>
      
    </head>
    
  <body>
  
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <div class="panel-heading">Calendar Example</div>
          
          <div class="panel-body">
            {!! $calendar->calendar() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
  
  </body>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="fullcalendar.js"></script>
    <script src='locale/pt-br.js'></script>

    {!! $calendar->script() !!}

</html>
